Fala dev! Tudo bem?

Aqui estão as instruções para realização do teste técnico, este teste tem o objetivo de avaliar e desafiar você, 
estamos em busca de profissionais interessados em aprender! Queremos apenas conhecer seu esforço! Vamos lá?! :)

Fork esse repositório;
Para a realização do teste, sua missão será criar uma ou mais páginas, sendo o layout todo livre. Divirta-se! 
Você deverá usar UMA dessas API's: [ { linkApi: 'https://swapi.co/' }, { linkApi: 'https://developer.marvel.com/' },
{ linkApi: 'https://punkapi.com/documentation/v2' }, { linkApi: 'https://developer.spotify.com/documentation/web-api/' }, ];
E claro, todo o desenvolvimento utilizando ReactJs. \o/


Se você quiser dar uma turbinada no seu projeto, você pode utilizar:
Material Design;
Criar o CSS usando Styled-components;
Utilizar Rotas;
Você pode também criar um componente passando para ele props. Isso seria muito legal! 
Seria maravilhoso também se o seu layout fosse responsivo, ok?! Pense nisso com carinho! :D


You can do it! 👍














